# Spring For Beginner


In questo repo, troverete le slide del corso.
All'inizio di ogni lezione sarà disponibile una nuova slide.

Per la prima lezione il tema è Spring Core e le slide saranno supportate da progetti di prova preparati a posteriori ed esercitazioni svolte in aula.

Link Utili per il software necessario al corso:

[IntelliJ](https://www.jetbrains.com/idea/download/#section=windows)
[Eclipse STS](https://spring.io/tools)
[PostgreSQL](https://www.postgresql.org/download/)
[Maven 3](https://maven.apache.org/download.cgi)
[JDK 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)